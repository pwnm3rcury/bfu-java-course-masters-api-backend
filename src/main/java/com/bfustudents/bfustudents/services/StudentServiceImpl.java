package com.bfustudents.bfustudents.services;

import com.bfustudents.bfustudents.db.entities.Student;
import com.bfustudents.bfustudents.db.entities.StudyProgram;
import com.bfustudents.bfustudents.db.repositories.StudentRepository;
import com.bfustudents.bfustudents.db.repositories.StudyProgramRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final StudyProgramRepository studyProgramRepository;

    @Autowired
    public StudentServiceImpl(StudentRepository studentRepository, StudyProgramRepository studyProgramRepository) {
        this.studentRepository = studentRepository;
        this.studyProgramRepository = studyProgramRepository;
    }

    @Override
    public List<Student> getAll() {
        return studentRepository.findAll();
    }

    @Override
    @Transactional
    public Student create(Student student) {
        StudyProgram studyProgram = studyProgramRepository.findById(student.getStudyProgram().getId()).orElseThrow(() -> new IllegalArgumentException("No such Study Program"));
        student.setStudyProgram(studyProgram);
        return studentRepository.save(student);
    }

    @Override
    public Student getById(Integer id) {
        return studentRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public Student update(Integer id, Student student) {
        Student _student = studentRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("No such Student"));
        StudyProgram studyProgram = studyProgramRepository.findById(student.getStudyProgram().getId()).orElseThrow(() -> new IllegalArgumentException("No such Study Program"));

        _student.setFirstName(student.getFirstName());
        _student.setLastName(student.getLastName());
        _student.setStudentNumber(student.getStudentNumber());
        _student.setYearOfStudy(student.getYearOfStudy());
        _student.setFormOfEducation(student.getFormOfEducation());
        _student.setStudyProgram(studyProgram);
        return studentRepository.save(_student);
    }
}
