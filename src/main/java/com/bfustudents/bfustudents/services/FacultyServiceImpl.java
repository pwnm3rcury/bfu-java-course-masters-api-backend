package com.bfustudents.bfustudents.services;

import com.bfustudents.bfustudents.db.entities.Faculty;
import com.bfustudents.bfustudents.db.repositories.FacultyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FacultyServiceImpl implements FacultyService {

    private final FacultyRepository facultyRepository;

    @Autowired
    public FacultyServiceImpl(FacultyRepository facultyRepository) {
        this.facultyRepository = facultyRepository;
    }

    @Override
    public List<Faculty> getAll() {
        return facultyRepository.findAll();
    }

    @Override
    public Faculty create(Faculty faculty) {
        return facultyRepository.save(faculty);
    }

    @Override
    public Faculty getById(Integer id) {
        return facultyRepository.findById(id).orElse(null);
    }

    @Override
    public Faculty update(Integer id, Faculty faculty) {
        Faculty _faculty = facultyRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("No such Faculty"));

        _faculty.setName(faculty.getName());
        return facultyRepository.save(_faculty);
    }
}
