package com.bfustudents.bfustudents.services;

import com.bfustudents.bfustudents.db.entities.StudyProgram;

import java.util.List;

public interface StudyProgramService {
    List<StudyProgram> getAll();

    StudyProgram create(StudyProgram studyProgram);

    StudyProgram getById(Integer id);

    StudyProgram update(Integer id, StudyProgram studyProgram);
}
