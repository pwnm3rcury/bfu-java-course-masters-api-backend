package com.bfustudents.bfustudents.services;

import com.bfustudents.bfustudents.db.entities.Faculty;

import java.util.List;

public interface FacultyService {
    List<Faculty> getAll();

    Faculty create(Faculty faculty);

    Faculty getById(Integer id);

    Faculty update(Integer id, Faculty faculty);

}
