package com.bfustudents.bfustudents.controllers;

import com.bfustudents.bfustudents.db.entities.Faculty;
import com.bfustudents.bfustudents.db.entities.Student;
import com.bfustudents.bfustudents.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudentController {
    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @CrossOrigin
    @GetMapping("/students")
    public List<Student> studentGet() {
        return studentService.getAll();
    }

    @CrossOrigin
    @GetMapping("/students/{id}")
    public Student studentGetOne(@PathVariable Integer id) {
        return studentService.getById(id);
    }

    @CrossOrigin
    @PostMapping("/students")
    public Student studentCreate(@RequestBody Student student) {
        return studentService.create(student);
    }

    @CrossOrigin
    @PutMapping("/students/{id}")
    public Student studentUpdate(@PathVariable Integer id, @RequestBody Student student) {
        return studentService.update(id, student);
    }

}
