package com.bfustudents.bfustudents.controllers;

import com.bfustudents.bfustudents.db.entities.Faculty;
import com.bfustudents.bfustudents.services.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FacultyController {
    private final FacultyService facultyService;

    @Autowired
    public FacultyController(FacultyService facultyService) {
        this.facultyService = facultyService;
    }

    @CrossOrigin
    @GetMapping("/faculties")
    public List<Faculty> facultiesGet() {
        return facultyService.getAll();
    }

    @CrossOrigin
    @GetMapping("/faculties/{id}")
    public Faculty facultyGetOne(@PathVariable Integer id) {
        return facultyService.getById(id);
    }

    @CrossOrigin
    @PostMapping("/faculties")
    public Faculty facultyCreate(@RequestBody Faculty faculty) {
        return facultyService.create(faculty);
    }

    @CrossOrigin
    @PutMapping("/faculties/{id}")
    public Faculty facultyUpdate(@PathVariable Integer id, @RequestBody Faculty faculty) {
        return facultyService.update(id, faculty);
    }

}
