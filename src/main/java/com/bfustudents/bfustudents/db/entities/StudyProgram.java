package com.bfustudents.bfustudents.db.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name = "study_program")
public class StudyProgram {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;

    @ManyToOne
    @JoinColumn(name = "faculty_id", nullable = false)
    private Faculty faculty;

    private String degree;
    private Integer typeOfStudy;
    private String lengthOfStudy;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public Integer getTypeOfStudy() {
        return typeOfStudy;
    }

    public void setTypeOfStudy(Integer typeOfStudy) {
        this.typeOfStudy = typeOfStudy;
    }

    public String getLengthOfStudy() {
        return lengthOfStudy;
    }

    public void setLengthOfStudy(String lengthOfStudy) {
        this.lengthOfStudy = lengthOfStudy;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
