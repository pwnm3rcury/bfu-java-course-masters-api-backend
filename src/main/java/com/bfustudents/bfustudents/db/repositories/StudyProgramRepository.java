package com.bfustudents.bfustudents.db.repositories;

import com.bfustudents.bfustudents.db.entities.StudyProgram;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudyProgramRepository extends JpaRepository<StudyProgram,Integer> {
}
