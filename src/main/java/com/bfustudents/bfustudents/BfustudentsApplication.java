package com.bfustudents.bfustudents;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BfustudentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BfustudentsApplication.class, args);
	}

}
